@extends('layout.master')
@section('judul')
Halaman Form
@endsection

@section('content')
<h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="POST">
        @csrf
  <label for="fname">First name:</label><br>
  <input type="text" id="fname" name="fname"><br><br>
  <label for="lname">Last name:</label><br>
  <input type="text" id="lname" name="lname"><br><br>
  <label for="gender">Gender</label><br><br>
  <input type="radio" name="gender" value="male"> Male<br>
  <input type="radio" name="gender" value="female"> Female<br><br>
  <label for="national">Nationality</label><br><br>
  <select name="national" id="national">
  <option value="ina">Indonesia</option>
  <option value="Mala">Malaysia</option>
  <option value="sig">Singapura</option>
  </select><br><br>
  <label for="language">Language Spoken</label><br><br>
  <input type="checkbox" id="language" >Indonesia<br>
  <input type="checkbox" id="language" >English<br>
  <input type="checkbox" id="language" >Other<br><br>
  <label for="bio">Bio</label><br><br>
  <textarea id="txtid" name="txtname" rows="10" cols="30" maxlength="200"></textarea><br><br>
  <input type="submit" value="submit" >
</form>
@endsection
