@extends('layout.master')
@section('judul')
Halaman Update Profile
@endsection

@section('content')
<form action='/profile/{{$profile->id}}' method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control"  value="{{$profile->user->name}}" disabled >
  </div>  
  
  <div class="form-group">
    <label>Email</label>
    <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
  </div>  
  
  <div class="form-group">
    <label>Age</label>
    <input type="number" class="form-control" name="age" value="{{$profile->age}}" >
  </div>  
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" >{{$profile->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
<div class="form-group">
    <label>Adress</label>
    <textarea class="form-control" name="adress" >{{$profile->adress}}</textarea>
  </div>
  @error('adress')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection