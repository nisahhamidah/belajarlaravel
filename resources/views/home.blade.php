@extends('layout.master')
@section('judul')
Halaman Home
@endsection

@section('content')
  <h1>Media Online</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
    <h3>Benefit Belajar di Media Online</h3>
<ol>
  <li>Mendapatkan Motivasi dari sesama Developer</li>
  <li>Sharing Knowledge</li>
  <li>Dibuat oleh calon webdeveloper terbaik</li>
</ol>
    <h3>Cara Bergabung ke Media Online</h3>
    <ul>
  <li>Mengunjungi Website ini</li>
  <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
  <li>Selesai</li>
</ul>

@endsection
    