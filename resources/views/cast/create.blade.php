@extends('layout.master')
@section('judul')
Tambah cast
@endsection

@section('content')
<form action='/cast' method="POST">
    @csrf
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" >
  </div>  
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Age</label>
    <input type="text" name="age" class="form-control">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror 
  <div class="form-group">
    <label>Bio</label>
    <input type="textarea" class="form-control" name="bio" >
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection
    