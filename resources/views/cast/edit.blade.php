@extends('layout.master')
@section('judul')
Edit cast
@endsection

@section('content')
<form action='/cast/{{$cast->id}}' method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Name</label>
    <input type="text" class="form-control" name="name" value="{{$cast->name}}" >
  </div>  
  @error('name')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Age</label>
    <input type="text" name="age" class="form-control" value="{{$cast->age}}">
  </div>
  @error('age')
    <div class="alert alert-danger">{{ $message }}</div>
  @enderror 
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" >{{$cast->bio}}</textarea>
  </div>
  @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
  <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
    