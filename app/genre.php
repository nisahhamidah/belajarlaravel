<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class genre extends Model
{
    protected $table = 'genres';
    protected $fillable =['name'];

    public function film()
    {
        return $this->hasMany('App\film');
    }
}
