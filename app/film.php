<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class film extends Model
{
    protected $table = 'films';
    protected $fillable =['title','sumamary','year','poster','genre_id'];

    public function genre()
    {
        return $this->belongsTo('App\genre');
    }

    public function peran()
    {
        return $this->hasMany('App\peran');
    }
    public function kritik()
    {
        return $this->hasMany('App\kritik');
    }
}
