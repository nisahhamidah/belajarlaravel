<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kritik extends Model
{
    protected $table = 'kritik';
    protected $fillable =['content','point','user_id','film_id'];

    public function film()
    {
        return $this->belongsTo('App\film');
    }
    public function user()
    {
        return $this->belongsTo('App\cast');
    }
}
