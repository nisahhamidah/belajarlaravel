<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class peran extends Model
{
    protected $table = 'peran';
    protected $fillable =['film_id','cast_id'];

    public function film()
    {
        return $this->belongsTo('App\film');
    }
    public function cast()
    {
        return $this->belongsTo('App\cast');
    }
}
