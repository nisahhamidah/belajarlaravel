<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\profile;

class ProfileController extends Controller
{
    public function index(){
        $profile = profile::where('user_id', Auth::id())->first();
        
        return view('profile.index',compact('profile'));
    }
    public function update(Request $request, $id){
        $request->validate([
        'age' => 'required',
        'bio' => 'required',
        'adress' => 'required',
     ]);

     $profile = profile::find($id);
 
     $profile->age = $request['age'];
     $profile->bio = $request['bio'];
     $profile->adress = $request['adress'];

     $profile->save();

     return redirect('/profile');
    }
}
