<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cast extends Model
{
    protected $table = 'casts';
    protected $fillable =['name','age','bio'];

    public function peran()
    {
        return $this->hasMany('App\peran');
    }
}
