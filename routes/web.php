<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@home');
Route::get('/register', 'AuthController@regis');
Route::post('welcome','AuthController@kirim');
Route::get('/data-table', function () {
    return view ('table.data-table');
});

Route::group(['middleware' => ['auth']], function () {
    // CRUD Kategori
// cread
Route::get('/cast/create','CastController@create'); //form menuju form create
Route::post('/cast', 'CastController@store'); //route untuk menyimpan data ke database

// Read
Route::get('/cast','CastController@index'); //route list kategori
Route::get('/cast/{cast_id}','CastController@show'); //Route detail cast

// Update
Route::get('/cast/{cast_id}/edit', 'CastController@edit'); //Route menuju form edit
Route::put('/cast/{cast_id}', 'CastController@update'); //Route untu update berdasarkan id di database  

//Delete
Route::delete('/cast/{cast_id}', 'CastController@destroy'); //Route untuk hapus data di database
//Update Profile
Route::resource('/profile','ProfileController')->only(['index','update']);

});

Auth::routes();
